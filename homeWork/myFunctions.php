<?php
namespace myFoo;

//Задача5:
// для каждой вашей функции, чтобы показать ее работу;
//все ваши функции и примеры напишите в отдельный файл с
// названием myFunctions.php и определите в отдельный неймспейс;
//пример: у меня попалась функция coun(),
// я создаю несколько разных массивов, используя эту функцию показываю,
// как она работает - вывожу на экран сам массив и к-тво элементов в нем,
// подсчитанное этой функцией;
//каждый сделает свою часть и посмотрит эту же задачу у остальных,
// таким образом все узнают, как работает та или иная функция;


//lcfirst();
//ucfirst();
//htmlspecialchars();
//array_intersect();
//ksort();
//ceil();
//time();
//isset();
//empty();


function foo() {
    $string_1 = 'Assasin';
    echo lcfirst($string_1);
}

function foo1() {

    $string_2 = 'assasin';
    echo ucfirst($string_2);
}

function html() {
    $string_3 = 'Hello every body ';
    echo htmlspecialchars($string_3, ENT_XML1);
}

function inspect() {

    $array1 = ["a" => "green", "red", "blue"];
    $array2 = ["b" => "green", "yellow", "red"];
    $result = array_intersect($array1, $array2);
    print_r($result);
}

function sorts() {
    $fruits = ["d"=>"lemon", "a"=>"orange", "b"=>"banana", "c"=>"apple"];
    ksort($fruits);
    foreach ($fruits as $key => $val) {
        echo "$key = $val\n";
    }
}

function flo() {
    $flo1 = 15.269;
    echo ceil($flo1);
}

function timmes()
{
    $nextWeek = time() + (7 * 24 * 60 * 60);
    // 7 дней; 24 часа; 60 минут; 60 секунд
    echo 'Сейчас:           ' . date('Y-m-d') . "\n";
    echo 'Следующая неделя: ' . date('Y-m-d', $nextWeek) . "\n";
// или с помощью strtotime():
    echo 'Следующая неделя: ' . date('Y-m-d', strtotime('+1 week')) . "\n";
}

function isse($var) {
    $var;
// Проверка вернет TRUE, поэтому текст будет напечатан.
    if (isset($var)) {
        echo "Эта переменная определена, поэтому меня и напечатали.";
    }
}

function emmpt($var) {
    $var;
// Принимает значение true, потому что $var пусто
    if (empty($var)) {
        echo '$var или 0, или пусто, или вообще не определена';
    }

}