<?php
require_once 'mathFunctions/functionDistans.php';
require_once 'mathFunctions/workforArray.php';
require_once 'myFunctions.php';

//declare(strict_types=1);
    /**
     * Created by PhpStorm.
     * User: vitalikadmin13
     * Date: 26.03.19
     * Time: 12:52
     */
    ini_set('error_reporting', E_ALL);
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);

    echo "TASK 1 => ";

    echo '<pre>' . PHP_EOL . '<pre/>';

//1)округлите число, записанное в переменную $number,
// до двух знаков после запятой и выведите результат на экран;
    $number = 1.514558;

    echo round($number, 2);

    echo '<pre>' . PHP_EOL . '<pre/>';


    echo "TASK 2 => ";
//2)дана строка 'php', сделайте из нее строку 'PHP';
    $string = 'php';

    echo strtoupper($string);

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 3 => ";
//3)дана строка 'PHP', сделайте из нее строку 'php';
    $string1 = 'PHP';

    echo ucfirst(strtolower($string1));

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 4 => ";
//4)напишите функцию, которая принимает строку с названиями 4 овощей для салата через комму,
//а возвращает массивы из 4 элементов, вызовите функцию и поместите все 4 элемента в отдельную переменную
// в одно действие;

    $str = 'tomato, cucumber,  salad, cabbage ';

    function salad($string): array
    {

        $arr = explode(',', $string);
        return $arr;
    }

    $result = salad($str);
    print_r($result);

    echo '<pre>' . PHP_EOL . '<pre/>';


    echo "TASK 5 => ";
//5)дана строка '31.12.2013', замените все точки на дефисы;
//
    $dates = '31.12.2013';
//
    function dat($dat)
    {

//Разбиваем строку в массив по разделителю '-':
        $arr = explode('.', $dat);

//Получим дату в нужном формате:
        echo $arr[0] . '-' . $arr[1] . '-' . $arr[2];

    }

    dat($dates);

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 6 => ";
//6)есть переменная $password, в которой хранится пароль пользователя,
// напишите функцию, которая выведите пользователю сообщение о том, что если количество
//  символов пароля больше 5-ти и меньше 10-ти, то пароль подходит, иначе сообщение о том,
//  что нужно придумать другой пароль;


    function password()
    {
        $password = '123456789';
        $length = strlen($password);

        if ($length > 5 || $length < 10) {
            echo 'Пароль подходит';
        } else {
            echo 'Пароль не подходит, придумайте другой';
        }
    }

    password();

    echo '<pre>' . PHP_EOL . '<pre/>';


    echo "TASK 7 => ";
//7)дана строка '1234567890', разбейте ее на массив с элементами по 2 цифры в каждом;

    $stringNunber = '1234567890';

    function strTransform($strTr)
    {
        print_r(str_split($strTr, 2));
    }

    strTransform($stringNunber);

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 8 => ";
//8)сделайте функцию, которая принимает параметром число от 1 до 7,
// а возвращает день недели на русском языке.
//

    function day($num)
    {
        $day = ['Пн', 'Вт', 'Ср', 'Чет', 'Пят', 'Суб', 'Воскр'];
        echo $day[$num - 1];//потому-что с 0 отчет
    }

    echo day(7);

    echo '<pre>' . PHP_EOL . '<pre/>';


    echo "TASK 9 => ";

//9)дан массив 'a'=>1, 'b'=>2, 'c'=>3, поменяйте в нем местами ключи и значения;

    function arrfli()
    {
        $array1 = ['a' => 1, 'b' => 2, 'c' => 3];
        print_r(array_flip($array1));
    }

    arrfli();

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 10 => ";

//10)найдите все счастливые билеты, счастливый билет - это билет,
// в котором сумма первых трех цифр его номера равна сумме вторых трех цифр его номера,
// функция должна принимать номер билета и возвращать true или false;
//

    $ticket = 112103;

    function HappyTicket($tick)
    {

        $tick = (string)$tick;
        if ($tick[0] + $tick[1] + $tick[2] == $tick[3] + $tick[4] + $tick[5]) {
            return "true";
        } else {
            return "false";
        }
    }

    echo 'Этот билет' . ' ' . HappyTicket($ticket);


    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 11 => ";

//11)сделайте функцию, которая отнимает от первого числа второе и делит на третье;

    function calculation($firstNum = 0, $secondNum = 0, $thirdNum = 1)
    {
        return ($firstNum - $secondNum) / $thirdNum;
    }

    echo calculation(20, 10, 2) . '<br>';

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 12 => ";

//12)дан массив с элементами 1, 2, 3, 4, 5,
// с помощью функции array_slice 	создайте из него массив $result с элементами 2, 3, 4;

    $arrayNumber = [1, 2, 3, 4, 5];

    function createArray($arr)
    {

        $result = array_slice($arr, 1, 3);

        print_r($result);

    }

    createArray($arrayNumber);

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 13 => ";

//13)есть строка, проверьте через функцию, что она начинается на
// 'http://' или на 'https://', если это так, выведите 'да', если не так - 'нет';

    $strHttp = 'https';

    function checkString($str)
    {
        if (substr($str, 0, 5) == 'https') {
            echo 'YES';
        } else {
            echo 'NO';
        }
        return $str;
    }

    checkString($strHttp);


    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 14 => ";

//14)дана строка $str, замените в ней все буквы 'a' на цифру 1,
// буквы 'b' - на 2, а буквы 'c' - на 3,
// решите задачу двумя способами работы с функцией strtr (массив замен и две строки замен);


    function change()
    {
        $str1 = 'a, b, c';
        echo strtr($str1, ['a' => 1, 'b' => '2', 'c' => '3']);
    }

    change();

    echo '<pre>' . PHP_EOL . '<pre/>';

    function change2()
    {
        $str = 'a, b, c';
        echo strtr($str, 'a,b,c', '1,2,3');
    }

    change2();


    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 15 => ";

//15)сделайте функцию getFormattedDate,
// которая принимает на вход три параметра:
// день, месяц и год рождения, а возвращает их строкой в отформатированном виде,
// например: 30:02:1953, день и месяц нужно форматировать так,
// чтобы при необходимости добавлялся 0 слева.
// (например, если в качестве месяца пришла цифра 7,
// то в выходной строке она должна быть представлена как 07), подсказка - sprintf;


    function getFormattedDate($day, $month, $year)
    {


        return sprintf($day . ':' . 0 . $month . ':' . $year);
    }


    echo getFormattedDate(13, 6, 1983);


    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 16 => ";

//16)напишите функцию (название придумайте сами),
// которая принимает два года рождения и возвращает строку с разницей в возрасте
// в виде The age difference is 11;

    function twoyears($years1, $years2)
    {

        if ($years1 >= $years2) {
            $results = $years1 - $years2;
            sprintf($years1);
            return "$years1 больше $years2 на $results";

        } else {
            $results = $years2 - $years1;
            sprintf($years2);
            return "$years2 больше $years1 на $results";
        }

    }

    echo twoyears(1980, 1958);


    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 17 => ";

//17)реализуйте функцию, которая округляет возраст так,
// что половина округляется в нижнюю сторону.
// То есть если человеку десять с половиной лет,
// то функция должна вернуть 10.
// Если ему хотя бы немного больше десяти с половиной, то округление идет в верхнюю сторону;

    function yearspiaple($yearpip)
    {
        return round($yearpip, 0, PHP_ROUND_HALF_DOWN);
    }

    echo yearspiaple(10.6);

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 18 => ";

//18)даны числа 4, -2, 5, 19, -130, 0, 10.
// Напишите функцию, которая вернет минимальное и максимальное число
// и запишите результат в 2 переменных за одну манипуляцию;


    $decNummin = [4, -200, 5, 19, -130, 0, 10];

    function resultNumbMin()
    {
        $decNummin = [4, -200, 5, 19, -130, 0, 10];
        $min = $decNummin[0];
        $max = $decNummin[0];
        for ($i = 1; $i < count($decNummin); $i++) {
            if ($decNummin[$i] < $min) {
                $min = $decNummin[$i];
            } elseif ($decNummin[$i] > $max) {
                $max = $decNummin[$i];
            }
        }

        return $min . ' ' . $max;
        //return $max;

    }

    echo resultNumbMin();

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 19 => 2 ";

//task 2 Задача2:
//Один ученый любит составлять карты, и ему часто нужно выводить на экран повторяющиеся символы
// для визуализации маршрутов.
//Например, так он иллюстрирует грунтовые дороги между городами: Нижние Варты =-=-=-=- Myr
//А так иллюстрирует магистрали: Киев ======== Чоп
//В документации PHP ученый нашёл функцию str_repeat(), которая возвращает повторяющуюся строку;
//Использую эту функцию, напишите свою, которая принимает 4 аргумента: 2 строки с названиями городов,
// количество повторений разделителя и сам разделитель,
// как не обязательный аргумент (по умолчанию ‘=’), который должен дублироваться;
//Цель функции - соединить 2 города с помощью дороги (повторяющегося) разделителя в одну строку;
//Используйте все возможные варианты строгой типизации.

    function linkCity()
    {
        echo 'kiev' . str_repeat("=", 10) . 'Lyviv';
    }

    linkCity();

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 19 => 3 ";

//task 3 Задача3:
//создайте папку mathFunctions в корне проекта, в нем создайте php-файл,
// в котором будут находится функции для работы с дистанцией,
// название файла придумайте сами, которое будет отвечать содержимому;
//в этом файле напишите функцию,
// которая будет считать дистанцию в километрах между двумя точками
// (2 числа передаваемые в аргументе) и возвращать эту дистанцию;
//используйте все возможные варианты строгой типизации;
//подключите данный файл в index.php и выведите на экран результат выполнения функции
// для 3 разных пар точек;

    echo distance();

    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 19 => 4 ";

//task 4 в папке mathFunctions в корне проекта, создайте php-файл,
// в котором будут находится функции для работы с массивами,
// название файла придумайте сами, которое будет отвечать содержимому;
//в этом файле напишите функцию,
// которая будет считать количество четных индексов
// в переданном массиве и возвращать это число;
//используйте все возможные варианты строгой типизации;
//в этом файле используйте namespace;
//подключите данный файл в index.php и выведите на экран
// результат выполнения функции для 2 разных массивов, используя namespace;


    echo 'Количество четных индексов ' . workForArr($arr= [4, 5, 6, 8]);



    echo '<pre>' . PHP_EOL . '<pre/>';

    echo "TASK 19 => 5 ";

//Задача5:
//есть файл - в нем список функций, которые нужно использовать в дз;
//у каждого студента по 5 функций + 3 функции обязательные для всех;
//ваша задача написать пример (а лучше парочку)
// для каждой вашей функции, чтобы показать ее работу;
//все ваши функции и примеры напишите в отдельный файл с
// названием myFunctions.php и определите в отдельный неймспейс;
//пример: у меня попалась функция coun(),
// я создаю несколько разных массивов, используя эту функцию показываю,
// как она работает - вывожу на экран сам массив и к-тво элементов в нем,
// подсчитанное этой функцией;
//каждый сделает свою часть и посмотрит эту же задачу у остальных,
// таким образом все узнают, как работает та или иная функция;

echo '<pre>' . PHP_EOL . '<pre/>';
myFoo\foo();
echo '<pre>' . PHP_EOL . '<pre/>';
myFoo\foo1();
echo '<pre>' . PHP_EOL . '<pre/>';

myFoo\html();
echo '<pre>' . PHP_EOL . '<pre/>';

myFoo\inspect();
echo '<pre>' . PHP_EOL . '<pre/>';

myFoo\sorts();
echo '<pre>' . PHP_EOL . '<pre/>';
myFoo\flo();
echo '<pre>' . PHP_EOL . '<pre/>';
myFoo\timmes();
echo '<pre>' . PHP_EOL . '<pre/>';
myFoo\isse($arr);
echo '<pre>' . PHP_EOL . '<pre/>';
$rrt = 0;
myFoo\emmpt($rrt);

echo '<pre>' . PHP_EOL . '<pre/>';

echo "TASK 19 => 6 => ";

//task 6 Задача6 (на рекурсию):
//дано какое-то число;
//сложите его цифры;
//если сумма получилась более 9-ти,
// опять сложите его цифры, и так, пока сумма не станет однозначным числом (9 и менее).

$num = 999998999;

function sequenceSum($number)
{
    if ($number < 10){
        echo "end";
        return;
    }
    $number = (string)$number;
    $sum = 0;
    for ($i = 0; $i < strlen($number); $i++) {
        $sum += $number[$i];
    }
    echo $sum . ", ";
    return sequenceSum((int)$sum);
//    $sum = $number[0] + $number[1];
}


sequenceSum($num);




