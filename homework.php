<?php
/**
 * Created by PhpStorm.
 * User: vitalikadmin13
 * Date: 26.03.19
 * Time: 12:56
 */


echo '<br/>';
echo 'Task 1 a => ';
$name = 'Bob'; // task 1 a
$age = 20;     // task 1 a
echo "Hello $name You are $age years old" . PHP_EOL; // task 1 b
echo '<br/>';
echo '<br/>';
echo '<br/>';
echo 'Task 2 b => ';
echo 'Hello Bob you are 20 years old';  //task 1 b

echo '<br/>';
echo '<br/>';

//echo 'Task 2 a => ';
echo '<br/>';
$x = (int)10;  //task 2 a
$y = (float)13.5; //task 2 a
$sum = $x + $y;  //task 2 b c
echo "TASK 2 d => ";
echo "The sum of $x and $y is $sum"; //task 2 d

echo '<br/>';
echo '<br/>';

echo "Task 3 a => ";
const STRING_SIMPLE = 'This is constant';  //task 3 a
const IN_DECIMA = 100;
const FL_DECIMA = 14.1;
echo '<br/>';
echo FL_DECIMA;
echo '<br/>';
echo IN_DECIMA;
echo '<br/>';
echo STRING_SIMPLE;    //task 3 b
echo '<br/>';

echo '<br/>';

echo "TAsk 4 => ";

$name = 'Vasiliy';   //task 4
$admin = $name;
echo $admin;
////////////////////////////

echo '<br/>';
echo '<br/>';
//////////////////////////////////////////////////////////////////////
echo "Task 5 a => ";

$third_planet = 'earth';  //task 5 a
$next_vizitor = 'Petya';  //task 5 b

echo $third_planet . PHP_EOL;
echo $next_vizitor;

echo '<br/>';
echo '<br/>';

//$a = 2; //task 6
//$x = 1 + (a *= 2); //х не будет равен нечему, выражение в скобках не выполнится."

echo '<br/>';
echo '<br/>';
//////////////////////////////////////////////////////////////
echo "Task 7 a => ";

$target = 55;   //task 7 a
if ($target == 55) {
    echo "corectly"; //variable equals $target
    echo '<br/>';
} else echo "is not corectly";
echo '<br/>';
echo '<br/>';
////////////////////////////////////////////////////////////
echo "Task 7 b ";

$target = 55;  //task 7 b
if ($target === 55) {
    echo "corectly"; //variable equals $target
    echo '<br/>';
} else echo "is not corectly";
echo '<br/>';
echo '<br/>';
////////////////////////////////////////////////////////////
echo "TASK 7 c =>";

$lowTarget = 5;   //task 7 c
if ($lowTarget <= 5) {
    echo "меньше или равно 5";
    echo '<br/>';
} else echo "is not corectly";


echo '<br/>';
echo '<br/>';
////////////////////////////////////////////////////////////
echo "TASK 7 d => ";

$minute = 30;   // task number 7 d
if ($minute <= 15) {
    echo "$minute попала в первую четверть часа";
} elseif ($minute <= 30 && $minute > 15) {
    echo "$minute попадает во вторую четверть";
} elseif ($minute <= 45 && $minute > 30) {
    echo "$minute попадает в 3 четверть";
} elseif ($minute <= 60 && $minute > 45) {
    echo "$minute попадает в 4 четверть";
} else echo "error";
echo '<br/>';
echo '<br/>';
////////////////////////////////////////////////////////////
echo "TASK 7 e => ";

$X = 2;  //task 7 e
$Y = 2;
if ($X <= 3 || $X > 10 && $Y >= 2 && $Y < 24) {
    echo "it is true";
} else echo "it is false";
echo '<br/>';
echo '<br/>';
///////////////////////////////////////////////////////////
echo 'Task 7 f => ';

$string = '18540'; //task 7 f
//$split = str_split($string);
//
//
//print_r ($split);
if ((int)$string[0] + (int)$string[1] === (int)$string[2] + (int)$string[3] + (int)$string[4]) {
//        echo $string[0] + $string[1];
    echo "TRUE";
} else echo "FALSE";

echo '<br/>';
echo '<br/>';
///////////////////////////////////////////////////////////////
echo 'Task 8 a => ';
$language = 'en_GB'; //task 8
$mounts = 0;

$moEng = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
$moRus = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'ауг', 'сен', 'окт', 'ноя', 'дек'];

if ($language == 'en_GB') {
    $mounts = $moEng;
}
if ($language == 'ru_RU') {
    $mounts = $moRus;
}
print_r($mounts);

echo '<br/>';
echo '<br/>';

echo 'Task 8 b => ';
$language = 'ru_RU';
$mounts = 0;

$moEng = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
$moRus = ['янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'ауг', 'сен', 'окт', 'ноя', 'дек'];

switch ($language) {
    case "en_GB":
        $mounts = $moEng;
        break;
    case "ru_RU":
        $mounts = $moRus;
        break;
}
print_r($mounts);

echo '<br/>';
//echo 'task 9 a => ';
echo '<br/>';
$arr = ['World, ',   //task 9 a
    '!',
    'Hello'
];

echo '<br/>';
echo 'task 9 b => ';
echo $arr[2] . " " . substr($arr[0], 0, -2) . $arr[1];  //task 9 b
echo '<br/>';

//    $temp = substr($arr[0], 0 , -2);
//echo 'abcd'[1];
//
//$string1 = '12111';
//$newArr = (int)explode('', $string1);//task 7 f
//echo $newArr;
echo '<br/>';
echo '<br/>';
echo 'Task 10 a => ';
$responseStatuses = [
    100 => 'Сообщения успешно приняты к отправке',
    105 => 'Ошибка в формате запроса',
    110 => 'Неверная авторизация',
    115 => 'Недопустимый IP-адрес отправителя',
];

echo $responseStatuses[105] . PHP_EOL;
echo $responseStatuses[110];

echo '<br/>';

// echo "Task 11 a => ";
$fruits = ['apple' => 3, 'orange' => 5, 'pear' => 10];

//$sum_fru = $fruits ['apple'] + ['orange'];
echo "Task 11 b => ";

$s1 = (int)$fruits['apple'] + (int)$fruits['orange'];
echo "Summa fructs = $s1";

echo '<br/>';
echo "Task 11 b => ";

$s2 = (int)$fruits['orange'] + (int)$fruits['pear'];
echo "Summa fructs= $s2";

echo '<br/>';
echo '<br/>';
//////////////////////////////////
echo "TASK 12 => ";
//echo '<br/>';

$arr = [
    'cms' => ['joomla', 'wordpress', 'drupal'],
    'colors' => ['blue' => 'голубой', 'red' => 'красный', 'green' => 'зеленый'],
];

echo $arr['colors']['red'];
echo ",";
echo $arr['cms'][1];
echo PHP_EOL;
echo $arr['colors']['green'];
echo ",";
echo $arr['cms'][2];


echo '<br/>';

echo '<br/>';


echo "TASK 13 => ";
echo '<br/>';

for ($i = 0; $i <= 75; $i++) {
    echo $i;
    echo '<br/>';
}
echo '<br/>';

echo '<br/>';

$n = 0;
//$count = 0;
while ($n <= 75) {
    echo '<br/>';
    echo $n;
    $n++;
}

echo '<br/>';
echo '<br/>';
//////////////////////////////
echo "TASK 14 => ";
echo '<br/>';

$nums = [7, 5, 3, 1];

for ($j = 0; $j < count($nums); $j++) {

    echo pow($nums[$j], 2);
    //$nums[$j]*$nums[$j];
    echo '<br/>';

}
echo '<br/>';
echo '<br/>';
echo "TASK 15 => ";

//$responseStatuses = [
//    100 => 'Сообщения успешно приняты к отправке',
//    105 => 'Ошибка в формате запроса',
//    110 => 'Неверная авторизация',
//    115 => 'Недопустимый IP-адрес отправителя',
//];
//for ($i = 0; $i < $count($responseStatuses); $i++){
//    echo $responseStatuses[$i]
//
//}


echo '<br/>';
echo '<br/>';
////advanced
///
echo "TASK 1 advanced";
echo '<br/>';
$num = 5000;
$count = 0;
while ($num >= 77) {
    $num = $num / 2;
    $count++;
    //echo $num.PHP_EOL;
}

echo $num;
echo '<br/>';
echo $count . PHP_EOL;

echo '<br/>';

do {
    $num = $num / 2;
    $count++;


} while ($num >= 77);

echo $num;
echo '<pre>' . PHP_EOL . '<pre/>';
echo "gggg";
echo '<pre>' . PHP_EOL . '<pre/>';
echo "gggg";

echo $count . PHP_EOL;

